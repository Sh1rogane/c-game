#ifndef SPLITBULLET_H
#define SPLITBULLET_H

#include "bullet.h"
#include "normalbullet.h"


class SplitBullet : public Bullet
{
	public:
		SplitBullet(double x, double y, double z, double splitTime, int splitCount, double size, int rotation , double speed);
		~SplitBullet();
		void update(double deltaTime);
		void render();
	protected:
	private:
		int splitCount; //Number of bullets that will come after split
		double timeSplit;
		double splitTime;//Time untill split
		Bullet* b;
		void split();
};

#endif // SPLITBULLET_H
