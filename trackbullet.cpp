#include "trackbullet.h"

TrackBullet::TrackBullet(double x, double y, double z, double splitTime, int splitCount, Player* p, double size, int rotation , double speed)
{
	Global::array.push_back(this);
	
	this->splitCount = splitCount;
	this->p = p;
	
    this->x = x;
    this->y = y;
    this->z = z;
    this->rotation = rotation;
    
    this->splitTime = splitTime;
    
    timeSplit = 0;
    layTime = 0;
    
    width = size;
    height = size;
    depth = size;
    
    this->speed = speed;
    
    calc();
    
    hitbox = new Hitbox(this, width, height, depth, width / 2, height / 2, depth / 2);
}
void TrackBullet::update(double deltaTime)
{
	//Adding the speed to the bullet so it will fly an direction.
    x += xSpeed * deltaTime;
    z += zSpeed * deltaTime;
    timeSplit += deltaTime;
    layTime += deltaTime;
    if(layTime > 0.02)
    {
    	layTime = 0;
		b = new SplitBullet(x,y,z,1,0,0.1,0,0);
		b = NULL;
    }
   
    if(timeSplit > splitTime)
    {
    	if(splitCount > 0)
    	{
    		split();
    	}
    	else
    	{
    		delete this;
    	}
    }
}
/*
* Calculatets where the player are and gives the new trackBullet right directions
*/
void TrackBullet::split()
{
	splitCount--;
	double dy = (p->getX()) - x;
	double dx = (p->getZ()) - z;

	double radians = atan2(dy,dx);
	double degrees = radians / (PI / 180);
	rotation = degrees - 90;
	b = new TrackBullet(x, y, z, splitTime, splitCount, p, width, rotation + 90, speed);
	b = NULL;
	delete this;
}
void TrackBullet::render()
{
	glPushMatrix();
	glColor3f(0, 0, 1);
	glTranslatef(x, y, z);
	glutSolidSphere(width,10,10);
	glPopMatrix();
	hitbox->updateHitbox();
}
TrackBullet::~TrackBullet()
{
	//dtor
	p = NULL;
}
