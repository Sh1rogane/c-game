#include "player.h"
#include <GL/glut.h>
#include "normalbullet.h"

Player::Player()
{
	up = false;
	down = false;
	left = false;
	right = false;
	space = false;
	jumping = false;
	mouseDown = false;
	isJumping = false;
	
	x = -15;
	y = 0;
	z = 8;
	width = 1;
	height = 1.5;
	depth = 1;
	//How much movment per second
	speed = 5;
	rotation = 0;
	bulletTime = 0;

	life = 50;
	
	gravity = 20;
	maxJumpSpeed = 20;
	jumpSpeed = 10;
	fallSpeed = 0;

	legRotation = 0;
	walkRotation = 1;
	hitbox = new Hitbox(this, width, height, depth, width / 2, 0, depth / 2);
}
Player::~Player()
{
}
void Player::mouseMove(int x, int y)
{
	int dy = x - (glutGet(GLUT_WINDOW_WIDTH) / 2);
	int dx = y - (glutGet(GLUT_WINDOW_HEIGHT) / 2);

	double radians = atan2(dy,dx);
	double degrees = radians / (PI / 180);
	rotation = degrees;
}
void Player::mouseClick(int button, int state, int x, int y)
{
    if(button == 0 && state == 0)
    {
        mouseDown = true;
    }
    else
    {
        mouseDown = false;
    }
}
void Player::update(double deltaTime)
{
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		Bullet* b = dynamic_cast<Bullet*>(Global::array[i]);
		if(b != NULL && hitbox->checkCollisions(b))
		{
			life--;
			delete b;
			b = NULL;
		}
	}
    bulletTime += deltaTime;
	if(up)
	{
		
		if(!hitbox->checkCollisions(0, 0.01, speed * deltaTime * -1))
		{
			z += speed * deltaTime * -1;
		}
	}
	else if(down)
	{
		if(!hitbox->checkCollisions(0, 0.01, speed * deltaTime))
		{
			z += speed * deltaTime;
		}
	}
	if(right)
	{
		if(!hitbox->checkCollisions(speed * deltaTime, 0.01, 0))
		{
			x += speed * deltaTime;
		}
	}
	else if(left)
	{
		if(!hitbox->checkCollisions(speed * deltaTime * -1, 0.01, 0))
		{
			x += speed * deltaTime * -1;
		}
	}
	if(mouseDown && bulletTime > 0.1)
	{
	    bulletTime = 0;
        double xSpeed = 1 * sin(rotation * (PI / 180));
		double zSpeed = 1 * cos(rotation * (PI / 180));
        b = new NormalBullet(this->x + xSpeed, this->y + 0.5, this->z + zSpeed, 0.1, this->rotation, 20);
		b = NULL;
	}
	jump(deltaTime);
	if(up||right||down||left)
	{
		legRotation += 150 * deltaTime * walkRotation;
		if(legRotation > 45)
		{
			walkRotation = -1;
		}
		else if(legRotation < -45)
		{
			walkRotation = 1;
		}
	}
	//Teleports player to lose room if he dies
	if(life <= 0)
	{
		x = -95;
		z = -95;
	}
}
/*
This function is handeling all the physics such as gravity and jumping and contains math,
collision detection and some other stuff that is hard to explain
*/
void Player::jump(double deltaTime)
{
	if(jumping)
	{
		fallSpeed -= gravity * deltaTime;
		//isJumping = true;
		if(!hitbox->checkCollisions(0, fallSpeed * deltaTime, 0) || y < 0)
		{
			y += fallSpeed * deltaTime;
		}

	}
	if(fallSpeed <= -maxJumpSpeed)
	{
		fallSpeed = -maxJumpSpeed;
	}
	Hitbox* h = hitbox->checkHitboxCollisions(0, fallSpeed * deltaTime, 0);
	if(h != NULL)
	{
		if(fallSpeed > 0)
		{
			fallSpeed = 0;
			y = (h->getY()) - height;
		}
		else
		{ 
			
			fallSpeed = 0;
			y = (h->getY()) + (h->getHeight());
			jumping = false;
		}
		h = NULL;
	}
	else if(y <= 0)
	{
		fallSpeed = 0;
		y = 0;
		jumping = false;
	}
	else
	{
		jumping = true;
	}
}
/*
Handeling all keyDown events
*/
void Player::keyDown(unsigned char key, int x, int y)
{
    //glutPostRedisplay();
    if(key == 'w')
    {
    	up = true;
    }
    else if(key == 's')
    {
    	down = true;
    }
    if(key == 'd')
    {
    	right = true;
    }
    else if(key == 'a')
    {
    	left = true;
    }
    if(key == ' ' && !space && !jumping)
    {
    	fallSpeed = jumpSpeed;
    	space = true;
    	jumping = true;
    	isJumping = true;
    }
    if(key == 27)
    {
        exit(0);
    }
}
/*
Handeling all keyUp events
*/
void Player::keyUp(unsigned char key, int x, int y)
{
    //glutPostRedisplay();
    if(key == 'w')
    {
    	up = false;
    }
    if(key == 's')
    {
    	down = false;
    }
    if(key == 'd')
    {
    	right = false;
    }
    if(key == 'a')
    {
    	left = false;
    }
    if(key == ' ')
    {
    	space = false;
    }
}
void Player::render()
{
	drawCharacter();
	glColor3f(0.0f, 1.0f, 0.0f);
	glPushMatrix();
	//Lifebar
	glTranslatef(x - width / 2, y + height, z + depth / 2);
	glRotatef(90,1,0,0);
	glRectf(0,0,life / 50 ,0.2);
	glColor3f(0.0f, 0.0f, 0.0f);
	glRectf(0,0, width,0.2);
	glPopMatrix();
	hitbox->updateHitbox();
}
void Player::drawCharacter()
{
	//Global matrix
	glPushMatrix();
	glTranslatef(x, y, z);
	glRotatef(rotation, 0, 1, 0);
	glColor3f(1.0f, 0.0f, 0.0f);
	
	//Left Leg
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(0, 1, 0);
	glRotatef(legRotation, 1,0,0);
	glTranslatef(-0.15, -0.4, 0);
	glScalef(0.3, 0.6, 0.2);
	glutSolidCube(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glutWireCube(1.0);
	glPopMatrix();
	
	//Right Leg
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(0, 1, 0);
	glRotatef(legRotation *-1, 1,0,0);
	glTranslatef(0.15, -0.4, 0);
	glScalef(0.3, 0.6, 0.2);
	glutSolidCube(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glutWireCube(1.0);
	glPopMatrix();
	
	//Body
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(0, 1.2, 0);
	glScalef(0.6, 0.6, 0.2);
	glutSolidCube(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glutWireCube(1.0);
	glPopMatrix();
	
	//Left Arm
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(0.4, 1.2, 0);
	glScalef(0.2, 0.5, 0.2);
	glutSolidCube(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glutWireCube(1.0);
	glPopMatrix();
	
	
	//Right Arm

	if(mouseDown)
	{
		glPushMatrix();
		glColor3f(1.0f, 0.0f, 0.0f);
		glTranslatef(-0.4, 1.4, 0.2);
		glScalef(0.2, 0.2, 0.5);
		glutSolidCube(1.0);
		glColor3f(0.0f, 0.0f, 0.0f);
		glutWireCube(1.0);
		glPopMatrix();
	}
	else
	{
		glPushMatrix();
		glColor3f(1.0f, 0.0f, 0.0f);
		glTranslatef(-0.4, 1.2, 0);
		glScalef(0.2, 0.5, 0.2);
		glutSolidCube(1.0);
		glColor3f(0.0f, 0.0f, 0.0f);
		glutWireCube(1.0);
		glPopMatrix();
	}
	
	//Head
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(0, 1.7, 0);
	glScalef(0.3, 0.3, 0.2);
	glutSolidCube(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glutWireCube(1.0);
	glPopMatrix();
	
	glPopMatrix();
}
