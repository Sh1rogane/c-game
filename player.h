#ifndef PLAYER_H
#define PLAYER_H

#include "mob.h"
#include "bullet.h"

class Player : public Mob
{
	public:
		Player();
		~Player();
		void render();
		void keyDown(unsigned char key, int x, int y);
		void keyUp(unsigned char key, int x, int y);
		void update(double deltaTime);
		void mouseMove(int x, int y);
		void mouseClick(int button, int state, int x, int y);
		
		
	protected:
	private:
		void drawCharacter();
		Bullet* b;
		bool mouseDown;
		double bulletTime;
		double legRotation;
		double walkRotation;
		
		//For jumping
		double gravity;
		bool space;
		bool jumping;
		bool isJumping;
		double jumpSpeed;
		double fallSpeed;
		double maxJumpSpeed;
		
		void jump(double deltaTime);
};

#endif // PLAYER_H
