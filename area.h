#ifndef AREA_H
#define AREA_H
#include "wall.h"
#include "enemy.h"
#include "global.h"
#include "player.h"
#include <iostream>
#include <vector>

class Area
{
	public:
		Area();
		virtual ~Area();
		virtual void createArea() = 0; //Creates the area
		virtual void update(double deltaTime) = 0;
		
		double getX()const{return x;}
		double getY()const{return y;}
		double getZ()const{return z;}
	protected:
		double x;
		double y;
		double z;
		
		Player* p;
		Wall* w;
		Wall* entrance;
		Wall* exit;
		Mob* e;
		
		bool isFinish;
		bool isInside;
		
		vector<Mob*> v;
		
		virtual void baseRoom(float r, float g, float b);
		bool checkIfFinish();
		bool checkIfInside();
		void closeDoors(double deltaTime);
		void openDoors(double deltaTime);
		virtual void createEnemys();
		
		
	private:
		
};

#endif // AREA_H
