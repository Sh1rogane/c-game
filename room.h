#ifndef ROOM_H
#define ROOM_H


class Room
{
	public:
		Room();
		virtual ~Room();
		virtual void render() = 0;
		virtual void update(double deltaTime) = 0;
	protected:
		int roomId;
	private:
};

#endif // ROOM_H
