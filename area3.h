#ifndef AREA3_H
#define AREA3_H

#include "area.h"


class Area3 : public Area
{
	public:
		Area3(Player* player, double x, double y, double z);
		~Area3();
		void createArea();
		void update(double deltaTime);
	protected:
	private:
		void createEnemys();
};

#endif // AREA3_H
