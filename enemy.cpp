#include "enemy.h"
#include "area.h"

Enemy::Enemy(Area* area, double x, double y, double z)
{
	up = false;
	down = false;
	left = false;
	right = false;
	
	this->area = area;
	
	this->x = x;
	this->y = y;
	this->z = z;
	
	life = 10;
	
	width = 1;
	height = 1;
	depth = 1;
	
	speed = 3;
	moveTime = 0;
	
	reloadTime = 0;
	
	rotation = 45;
	hitbox = new Hitbox(this, width, height, depth, width / 2, 0, depth / 2);
	findBox = new Hitbox(15, 3, 15,area->getX(), area->getY(), area->getZ());
	
	srand(time(NULL));
	
	Global::array.push_back(this);
}
Enemy::~Enemy()
{
	//Removes the enemy from the Global vector
	delete hitbox;
	hitbox = NULL;
	delete findBox;
	findBox = NULL;
	area = NULL;
	int pos = std::find(Global::array.begin(), Global::array.end(), this) - Global::array.begin();
	Global::array.erase(Global::array.begin() + pos);
	//dtor
}
void Enemy::render()
{
	//Character
	glColor3f(0.0f, 1.0f, 0.0f);
	glPushMatrix();
	glTranslatef(x, y + height / 2, z);
	glRotatef(rotation, 0, 1, 0);
	glutSolidTeapot(0.5);
	glPopMatrix();
	//Lifebar
	glColor3f(1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glTranslatef(x - width / 2, y + height, z - depth / 2);
	glRotatef(90,1,0,0);
	glRectf(0,0,life / 10,0.2);
	glColor3f(0.0f, 0.0f, 0.0f);
	glRectf(0,0, width,0.2);
	glPopMatrix();
	
	hitbox->updateHitbox();
	findBox->updateHitbox();
}
void Enemy::update(double deltaTime)
{
	//Loop to detect if it hits any bullets
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		Bullet* b = dynamic_cast<Bullet*>(Global::array[i]);
		if(b != NULL && hitbox->checkCollisions(b))
		{
			delete b;
			life--;
			b = NULL;
		}
		else
		{
			//finds where the player are
			Player* p = dynamic_cast<Player*>(Global::array[i]);
			if(p != NULL && this->findBox->checkCollisions(p))
			{
				double dy = (p->getX()) - x;
				double dx = (p->getZ()) - z;

				double radians = atan2(dy,dx);
				double degrees = radians / (PI / 180);
				rotation = degrees - 90;
				
				reloadTime += deltaTime;
				if(reloadTime > 0.5)
				{
					reloadTime = 0;
					reload = false;
				}
				if(!reload)
				{
					reload = true;
					double xSpeed = 1 * sin(rotation * (PI / 180));
					double zSpeed = 1 * cos(rotation * (PI / 180));
					NormalBullet* b = new NormalBullet(this->x + zSpeed, this->y + 0.1, this->z - xSpeed, 0.1, this->rotation + 90, 20);
					b = NULL;
					
				}
				p = NULL;
			}
			
		}
		
	}
	//Random movements
	moveTime += deltaTime;
	if(moveTime > 0.3)
	{
		moveTime = 0;
		int ran = rand() % 4;
		up = false;
		right = false;
		down = false;
		left = false;
		switch(ran)
		{
			case 0:
				up = true;
				break;
			case 1:
				right = true;
				break;
			case 2:
				down = true;
				break;
			case 3:
				left = true;
				break;
		}
	}
	
	if(up)
	{
		if(!hitbox->checkCollisions(0, 0.01, speed * deltaTime * -1))
		{
			z += speed * deltaTime * -1;
		}
	}
	else if(down)
	{
		if(!hitbox->checkCollisions(0, 0.01, speed * deltaTime))
		{
			z += speed * deltaTime;
		}
	}
	if(right)
	{
		if(!hitbox->checkCollisions(speed * deltaTime, 0.01, 0))
		{
			x += speed * deltaTime;
		}
	}
	else if(left)
	{
		
		if(!hitbox->checkCollisions(speed * deltaTime * -1, 0.01, 0))
		{
			x += speed * deltaTime * -1;
		}
	}
	if(life <= 0)
	{
		delete this;
	}
}
