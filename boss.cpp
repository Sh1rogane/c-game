#include "boss.h"

Boss::Boss(double x, double y, double z)
{
	//ctor
	up = false;
	down = false;
	left = false;
	right = false;
	
	this->x = x;
	this->y = y;
	this->z = z;
	
	life = 250;
	
	width = 2;
	height = 2.5;
	depth = 2;
	
	speed = 3;
	
	
	attackType = 0;
	reloadTime = 0;
	attackTime = 0;
	
	rotation = 45;
	hitbox = new Hitbox(this, width, height, depth, width / 2, 0, depth / 2);
	
	srand(time(NULL));
	
	Global::array.push_back(this);
}

Boss::~Boss()
{
	//Removes the enemy from the Global vector
	delete hitbox;
	hitbox = NULL;
	int pos = std::find(Global::array.begin(), Global::array.end(), this) - Global::array.begin();
	Global::array.erase(Global::array.begin() + pos);
}
void Boss::update(double deltaTime)
{
	//Change the attack each 5 seconds
	attackTime += deltaTime;
	if(attackTime > 5)
	{
		attackTime = 0;
		attackType = rand() % 3;
	}
	//Loop to detect if it hits any bullets
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		Bullet* b = dynamic_cast<Bullet*>(Global::array[i]);
		if(b != NULL && hitbox->checkCollisions(b))
		{
			delete b;
			life--;
			b = NULL;
		}
		else
		{
			//finds where the player are
			Player* p = dynamic_cast<Player*>(Global::array[i]);
			if(p != NULL)
			{
				double dy = (p->getX()) - x;
				double dx = (p->getZ()) - z;

				double radians = atan2(dy,dx);
				double degrees = radians / (PI / 180);
				rotation = degrees - 90;
				
				if(attackType == TS)
				{
					tripleShot(deltaTime);
				}
				else if(attackType == SS)
				{
					splitShot(deltaTime);
				}
				else if(attackType == HS)
				{
					trackShot(deltaTime, p);
				}
			}
			p = NULL;
		}
		
	}
	if(life <= 0)
	{
		delete this;
	}
}
void Boss::tripleShot(double deltaTime)
{
	reloadTime += deltaTime;
	if(reloadTime > 0.5)
	{
		reloadTime = 0;
		reload = false;
	}
	if(!reload)
	{
		reload = true;
		double zSpeed = 1 * sin(rotation * (PI / 180));
		double xSpeed = 1 * cos(rotation * (PI / 180));
		Bullet* b = new NormalBullet(this->x + xSpeed * width, this->y + 0.3, this->z - zSpeed * width, 0.2, this->rotation + 90, 10);
		b = new NormalBullet(this->x + xSpeed * width, this->y + 0.3, this->z - zSpeed * width, 0.2, this->rotation + 90 + 15, 10);
		b = new NormalBullet(this->x + xSpeed * width, this->y + 0.3, this->z - zSpeed * width, 0.2, this->rotation + 90 - 15, 10);
		b = NULL;
		
	}
	
}
void Boss::splitShot(double deltaTime)
{
	reloadTime += deltaTime;
	if(reloadTime > 1.5)
	{
		reloadTime = 0;
		reload = false;
	}
	if(!reload)
	{
		reload = true;
		double zSpeed = 1 * sin(rotation * (PI / 180));
		double xSpeed = 1 * cos(rotation * (PI / 180));
		Bullet* b = new SplitBullet(x + xSpeed * width, 0.3, z - zSpeed * width, 0.3, 50, 0.2, rotation + 90, 10);
		b = NULL;
		
	}
	
}
void Boss::trackShot(double deltaTime, Player* p)
{
	reloadTime += deltaTime;
	if(reloadTime > 1.5)
	{
		reloadTime = 0;
		reload = false;
	}
	if(!reload)
	{
		reload = true;
		double zSpeed = 1 * sin(rotation * (PI / 180));
		double xSpeed = 1 * cos(rotation * (PI / 180));
		Bullet* b = new TrackBullet(x + xSpeed * width, 0.3, z - zSpeed * width, 0.5, 5, p, 0.2, rotation + 90, 10);
		b = NULL;
		
	}
}
void Boss::render()
{
	//Character
	glColor3f(0.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslatef(x, y + height / 2, z);
	glRotatef(rotation, 0, 1, 0);
	glutSolidTeapot(1.5);
	glPopMatrix();
	//Lifebar
	glPushMatrix();
	glTranslatef(x - width / 2, y + height, z - depth / 2);
	glRotatef(90,1,0,0);
	glColor3f(1.0f, 0.0f, 0.0f);
	glRectf(0,0,(life / 125) ,0.2);
	glColor3f(0.0f, 0.0f, 0.0f);
	glRectf(0,0, width,0.2);
	glPopMatrix();
	
	hitbox->updateHitbox();
}
