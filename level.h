#ifndef LEVEL_H
#define LEVEL_H
#include <iostream>
#include <GL/glut.h>
#include <stdlib.h>
#include <vector>
#include "startarea.h"
#include "area1.h"
#include "area2.h"
#include "area3.h"
#include "area4.h"
#include "bossarea.h"
#include "player.h"

using namespace std;

class Level
{
	public:
		Level(Player* player);
		~Level();
		void update(double deltaTime);
		void createLevel(); //Creates all areas
	protected:
	private:
		vector<Area*> areaVector; //Vector to keep all areas
		Player* player;
		Area* a;
		
};

#endif // LEVEL_H
