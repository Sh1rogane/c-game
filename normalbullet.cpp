#include "normalbullet.h"


NormalBullet::NormalBullet(double x, double y, double z, double size, int rotation, double speed)
{
	Global::array.push_back(this);
    this->x = x;
    this->y = y;
    this->z = z;
    this->rotation = rotation;
    width = size;
    height = size;
    depth = size;
    this->speed = speed;
    calc();
    hitbox = new Hitbox(this, width, height, depth, width / 2, height / 2, depth / 2);
    //hitbox(this);
}
NormalBullet::~NormalBullet()
{
    

}
void NormalBullet::render()
{
	glPushMatrix();
	glColor3f(0, 0, 1);
	glTranslatef(x, y, z);
	glutSolidSphere(width,10,10);
	glPopMatrix();
	hitbox->updateHitbox();
}
