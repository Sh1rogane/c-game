#ifndef TRACKBULLET_H
#define TRACKBULLET_H

#include "bullet.h"
#include "splitbullet.h"
#include "player.h"


class TrackBullet : public Bullet
{
	public:
		TrackBullet(double x, double y, double z, double splitTime, int splitCount, Player* p, double size, int rotation , double speed);
		~TrackBullet();
		void update(double deltaTime);
		void render();
	protected:
	private:
		int splitCount; //Number of times it will "bounce"
		double timeSplit;
		double splitTime; //Time untill "bounce"
		double layTime; //How many bullets it will lay behind
		Bullet* b;
		Player* p;
		void split();
};

#endif // TRACKSHOT_H
