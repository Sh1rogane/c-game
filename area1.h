#ifndef AREA1_H
#define AREA1_H

#include "area.h"


class Area1 : public Area
{
	public:
		Area1(Player* player, double x, double y, double z);
		~Area1();
		void createArea();
		void update(double deltaTime);
	protected:
	private:
		void createEnemys();
};

#endif // AREA1_H
