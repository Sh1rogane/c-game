#ifndef AREA4_H
#define AREA4_H

#include "area.h"


class Area4 : public Area
{
	public:
		Area4(Player* player, double x, double y, double z);
		~Area4();
		void createArea();
		void update(double deltaTime);
	protected:
	private:
		void createEnemys();
};	

#endif // AREA4_H
