#ifndef WALL_H
#define WALL_H

#include "entity.h"
#include "bullet.h"

class Wall : public Entity
{
    public:
        Wall(double x, double y, double z, double width, double height, double depth, float r, float g, float b);
        ~Wall();
        void render();
        void update(double deltaTime);
    protected:
    private:
		float r,g,b; //Colors
};

#endif // WALL_H
