#include "hitbox.h"
#include "entity.h"

Hitbox::Hitbox(Entity* entity, double width, double height, double depth, double offsetX, double offsetY, double offsetZ)
{
    //ctor
    fixedPos = false;
    
    this->entity = entity;
    this->xMovement = 0;
	this->yMovement = 0;
	this->zMovement = 0;
	
	this->offsetX = -offsetX;
	this->offsetY = -offsetY;
	this->offsetZ = -offsetZ;
	
	x = entity->getX() + offsetX;
	y = entity->getY() + offsetY;
	z = entity->getZ() + offsetZ;
	
	this->width = width;
	this->height = height;
	this->depth = depth;
	
}
Hitbox::Hitbox(double width, double height, double depth, double fixedX, double fixedY, double fixedZ)
{
    //ctor
    fixedPos = true;
    
    this->xMovement = 0;
	this->yMovement = 0;
	this->zMovement = 0;
	
	x = fixedX;
	y = fixedY;
	z = fixedZ;
	
	this->width = width;
	this->height = height;
	this->depth = depth;
	
}
Hitbox::~Hitbox()
{
    //dtor
    entity = NULL;
}
/*
*	Updates Hitbox x,y and z
*/
void Hitbox::updateHitbox()
{
	if(!fixedPos)
	{
		x = entity->getX() + offsetX;
		y = entity->getY() + offsetY;
		z = entity->getZ() + offsetZ;
	}
	//render();
}
bool Hitbox::checkCollisions(Entity* entity)
{
	Hitbox* a = this;
	Hitbox* b = entity->getHitbox();
	if((a->maxX() >= b->minX() && a->minX() <= b->maxX())
	&& (a->maxY() >= b->minY() && a->minY() <= b->maxY())
	&& (a->maxZ() >= b->minZ() && a->minZ() <= b->maxZ()))
	{
		a = NULL;
		b = NULL;
		return true;
	}
	return false;
}
/*
*	Checks if current entity will hit any hitbox the next frame
*	and if it will. Return true.
*/
bool Hitbox::checkCollisions(double xMovement, double yMovement, double zMovement)
{
	this->xMovement = xMovement;
	this->yMovement = yMovement;
	this->zMovement = zMovement;
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		if(Global::array[i] != entity)
		{
			Hitbox* a = this;
			Hitbox* b = Global::array[i]->getHitbox();
			if((a->maxX() >= b->minX() && a->minX() <= b->maxX())
			&& (a->maxY() >= b->minY() && a->minY() <= b->maxY())
			&& (a->maxZ() >= b->minZ() && a->minZ() <= b->maxZ()))
			{
				a = NULL;
				b = NULL;
				return true;
			}
		}
	}
	return false;
}
/*
*	Checks if current entity will hit any hitbox the next frame
*	and if it will. Return the Hitbox of the entity it will hit
*/
Hitbox* Hitbox::checkHitboxCollisions(double xMovement, double yMovement, double zMovement)
{
	this->xMovement = xMovement;
	this->yMovement = yMovement;
	this->zMovement = zMovement;
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		if(Global::array[i] != entity)
		{
			Hitbox* a = this;
			Hitbox* b = Global::array[i]->getHitbox();
			if((a->maxX() >= b->minX() && a->minX() <= b->maxX())
			&& (a->maxY() >= b->minY() && a->minY() <= b->maxY())
			&& (a->maxZ() >= b->minZ() && a->minZ() <= b->maxZ()))
			{
				a = NULL;
				return b;
			}
		}
	}
	return NULL;
}
/*
*	Resets current movement
*/
void Hitbox::reset()
{
	this->xMovement = 0;
	this->yMovement = 0;
	this->zMovement = 0;
}
void Hitbox::render()
{
	glPushMatrix();
	glColor3f(0, 0, 0);
	glTranslatef(x + xMovement + width / 2, y + yMovement + height / 2, z + zMovement + depth / 2);
	glScalef(width, height, depth);
	glutWireCube(1.0);
	glPopMatrix();
}
double Hitbox::minX()
{
	return x + xMovement;
}
double Hitbox::maxX()
{
	return x + width + xMovement;
}
double Hitbox::minY()
{
	return y + yMovement;
}
double Hitbox::maxY()
{
	return y + height + yMovement;
}
double Hitbox::minZ()
{
	return z + zMovement;
}
double Hitbox::maxZ()
{
	return z + depth + zMovement;
}
