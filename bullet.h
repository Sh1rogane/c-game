#ifndef BULLET_H
#define BULLET_H
#include "entity.h"


class Bullet : public Entity
{
	public:
        Bullet();
		~Bullet();
		virtual void render(){};
		virtual void update(double deltaTime);
	protected:
		void calc(); //Calculates the direction the bullet should fly
        int speed; //Speed of bullet
        int xSpeed; //The x direction(speed) of the bullet
        int zSpeed;//The z direction(speed) of the bullet
	private:
};

#endif // BULLET_H
