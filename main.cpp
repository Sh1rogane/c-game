#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <iostream>
#include <windows.h>
#include <GL/glut.h>
#include <stdlib.h>
#include "player.h"
#include "global.h"
#include "wall.h"
#include "level.h"
#include <vector>

using namespace std;

//Variables

int frame = 0;
//int time;
int timebase=0;


double lastFrame = 0;
double deltaTime = 0;
Player player;

Level level(&player);
bool fullscreen;

//Fuctions prototypes
static void resize(int width, int height);
static void render(void);
void update();
void keyDown(unsigned char key, int x, int y);
void keyUp(unsigned char key, int x, int y);
void passiveMouse(int x, int y);
void mouseClick(int button, int state, int x, int y);
//Ljus
const GLfloat light_ambient[]  = { 0.4f, 0.4f, 0.4f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 3.0f, 3.0f, 10.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };



/* GLUT callback Handlers */
int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,480);
    glutInitWindowPosition(500,100);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutCreateWindow("Spel");
	glEnable(GL_DEPTH_TEST);

    glutReshapeFunc(resize);
    glutDisplayFunc(render);
    glutKeyboardFunc(keyDown);
    glutKeyboardUpFunc(keyUp);
    glutIdleFunc(update);
    glutPassiveMotionFunc(passiveMouse);
    glutMotionFunc(passiveMouse);
    glutMouseFunc(mouseClick);
    
    //Ljus
	glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
    
    
    //Creates the lose room
    Wall w1(-100, 0, -100, 10, 4, 1, 1, 0, 0);
    Wall w2(-100, 0, -92, 11, 4, 1, 1, 0, 0);
    Wall w3(-100, 0, -100, 1, 4, 8, 1, 0, 0);
    Wall w4(-90, 0, -100, 1, 4, 8, 1, 0, 0);
    
	level.createLevel();
	Global::array.push_back(&player);
    glutMainLoop();

    return EXIT_SUCCESS;
}

static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}
void passiveMouse(int x, int y)
{
	player.mouseMove(x, y);
}
void mouseClick(int button, int state, int x, int y)
{
	player.mouseClick(button, state, x, y);
}
static void render(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective
	gluLookAt(player.getX(), 10 + player.getY(), player.getZ() + 5, player.getX(), player.getY(), player.getZ(), 0, 1, 0);
	glColor3f(0,0,0);
	glBegin(GL_QUADS);
		glVertex3f(-100,-0.1,-100);
		glVertex3f(100,-0.1,-100);
		glVertex3f(100,-0.1,100);
		glVertex3f(-100,-0.1,100);
	glEnd();
	//Looping the vector and rendering all entitys in it
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		Global::array.at(i)->render();
	}
	//Render the lose room
	glColor3f(1,1,1);
	glBegin(GL_QUADS);
		glVertex3f(-120,0,-120);
		glVertex3f(-80,0,-120);
		glVertex3f(-80,0,-80);
		glVertex3f(-120,0,-80);
	glEnd();

	//Draw Lose
	glPushMatrix();
	glColor3f(1,0,0);
	
	glTranslatef(-100,1,-95);
	glRotatef(-90,1,0,0);
	//L
	glRectf(2,0.5,2.5,3);
	glRectf(2,0.5,3.5,1);
	//O
	glRectf(4,1,4.5,3);
	glRectf(5,1,5.5,3);
	glRectf(4,0.5,5.5,1);
	glRectf(4,2.5,5.5,3);
	//S
	glRectf(6,2.5,7.5,3);
	glRectf(6,1.5,7.5,2);
	glRectf(6,0.5,7.5,1);
	glRectf(6,2,6.5,3);
	glRectf(7,1,7.5,2);
	//E
	glRectf(8,2.5,9,3);
	glRectf(8,1.5,9,2);
	glRectf(8,0.5,9,1);
	glRectf(8,0.5,8.5,3);
	glPopMatrix();
	
	glutSwapBuffers();
}


void keyDown(unsigned char key, int x, int y)
{
    player.keyDown(key, x, y);
}
void keyUp(unsigned char key, int x, int y)
{
    player.keyUp(key, x, y);
	if(key == 'f' && !fullscreen)
    {
    	fullscreen = true;
    	glutFullScreen();
    }
    else if(key == 'f' && fullscreen)
    {    	
    	fullscreen = false;
    	glutReshapeWindow(640,480);
    }
}
void update()
{
	//Calculate the frame time (used for time based movements)
	double now = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = (now - lastFrame) / 1000.0;
	lastFrame = now;

	frame++;
	//Writes out FPS and deltaTime each second
	if (now - timebase > 1000)
	{
		cout << "FPS: " << frame << " DELTA TIME: " << deltaTime << endl;
	 	timebase = now;
		frame = 0;
	}
	//Looping the vector and updateing all entitys in it
    for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		Global::array.at(i)->update(deltaTime);
	}
	level.update(deltaTime);
    glutPostRedisplay();
    Sleep(2);
}

/* Program entry point */


