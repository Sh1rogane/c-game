#include "bullet.h"

Bullet::Bullet()
{

}

Bullet::~Bullet()
{
	//Removes the bullet from the Global vector
	delete hitbox;
	hitbox = NULL;
	int pos = std::find(Global::array.begin(), Global::array.end(), this) - Global::array.begin();
	Global::array.erase(Global::array.begin() + pos);
}
void Bullet::update(double deltaTime)
{
	//Adding the speed to the bullet so it will fly an direction.
    x += xSpeed * deltaTime;
    z += zSpeed * deltaTime;
    //Checks if the bullet is outside
    if(z > 100 || x > 100 || x < -100 || z < -100)
    {
    	delete this;
    }
}
void Bullet::calc()
{
	xSpeed = speed * sin(rotation * (PI / 180));
	zSpeed = speed * cos(rotation * (PI / 180));
}
