#include "area1.h"

Area1::Area1(Player* player, double x, double y, double z)
{
	//ctor
	p = player;
	this->x = x;
	this->y = y;
	this->z = z;
	isFinish = false;
	isInside = false;
}

Area1::~Area1()
{
	//dtor
}
void Area1::update(double deltaTime)
{
	if(!isInside && checkIfInside())
	{
		isInside = true;
		createEnemys();
	}
	if(isInside && entrance->getY() < y && !isFinish) 
	{
		closeDoors(deltaTime);
	}
	if(!isFinish && isInside && checkIfFinish())
	{
		isFinish = true;
	}
	if(isFinish && entrance->getY() > y - 2 )
	{
		openDoors(deltaTime);
	}
}
void Area1::createArea()
{
	baseRoom(0.5,0.5,0.5);
}
void Area1::createEnemys()
{
	e = new Enemy(this, x + 13, y, z + 8);
	v.push_back(e);
}
