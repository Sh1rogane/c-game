#include "area2.h"

Area2::Area2(Player* player, double x, double y, double z)
{
	//ctor
	p = player;
	this->x = x;
	this->y = y;
	this->z = z;
	isFinish = false;
	isInside = false;
}

Area2::~Area2()
{
	//dtor
}
void Area2::update(double deltaTime)
{
	if(!isInside && checkIfInside())
	{
		createEnemys();
		isInside = true;
	}
	if(isInside && entrance->getY() < y && !isFinish) 
	{
		closeDoors(deltaTime);
	}
	if(!isFinish && isInside&& checkIfFinish())
	{
		isFinish = true;
	}
	if(isFinish && entrance->getY() > y - 2 )
	{
		openDoors(deltaTime);
	}
}
void Area2::createArea()
{
	baseRoom(0,0.5,0.5);
}
void Area2::createEnemys()
{
	e = new Enemy(this, x + 13, y, z + 5);
	v.push_back(e);
	e = new Enemy(this, x + 13, y, z + 13);
	v.push_back(e);
}
