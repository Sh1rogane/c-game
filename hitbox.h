#ifndef HITBOX_H
#define HITBOX_H
#include <iostream>
#include <GL/glut.h>


using namespace std;
class Entity;

class Hitbox
{
    
    protected:
    private:
        double x;
		double y;
        double z;
        double width;
		double height;
		double depth;
		Entity* entity;
        double offsetX;
        double offsetY;
        double offsetZ;
        
        double xMovement;
        double yMovement;
        double zMovement;
        
        bool fixedPos;
        

	public:
		Hitbox(Entity* entity, double width, double height, double depth, double offsetX, double offsetY, double offsetZ);
		Hitbox(double width, double height, double depth, double fixedX, double fixedY, double fixedZ);
		~Hitbox();
		void render();
		void updateHitbox();
		void reset();
		bool checkCollisions(double xMovement, double yMovement, double zMovement);
		bool checkCollisions(Entity* entity);
		Hitbox* checkHitboxCollisions(double xMovement, double yMovement, double zMovement);
		double getX()const{return x;}
		double getY()const{return y;}
		double getZ()const{return z;}
		double getWidth()const{return width;}
		double getHeight()const{return height;}
		double getDepth()const{return depth;}
		Entity* getEntity()const{return entity;}
		
		double minX();//Calulates minumum X position
		double maxX();//Calulates maximum X position
		double minY();//Calulates minumum Y position
		double maxY();//Calulates maximum Y position
		double minZ();//Calulates minumum Z position
		double maxZ();//Calulates maximum Z position
};

#endif // HITBOX_H
