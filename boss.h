#ifndef BOSS_H
#define BOSS_H

#include "mob.h"
#include "player.h"
#include "splitbullet.h"
#include "trackbullet.h"

#define TS 0 //Tripeshot
#define SS 1 //Splitshot
#define HS 2 //Homingshot(Trackshot)

class Boss : public Mob
{
	public:
		Boss(double x, double y, double z);
		~Boss();
		void render();
		void update(double deltaTime);
	protected:
	private:
		int attackType; //Wich attat that the boss will use
		bool reload; //If reloading
		double reloadTime; //Reload time
		double attackTime; //How long the boss will fire an attack
		void tripleShot(double deltaTime);//Function to shot triplebullets
		void splitShot(double deltaTime);//Function to shot splitbullets
		void trackShot(double deltaTime, Player* p);//Function to shot trackbullets
};

#endif // BOSS_H
