#include "level.h"

Level::Level(Player* player)
{
	//ctor
	this->player = player;
}

Level::~Level()
{
	//dtor
}
void Level::createLevel()
{
	a = new StartArea(0 - 17,0,0);
	a->createArea();
	a = new Area1(player, 0, 0, 0);
	a->createArea();
	areaVector.push_back(a);
	a = new Area2(player,0 + 17,0,0);
	a->createArea();
	areaVector.push_back(a);
	a = new Area3(player,0 + 17 + 17,0,0);
	a->createArea();
	areaVector.push_back(a);
	a = new Area4(player,0 + 17 + 17 + 17,0,0);
	a->createArea();
	areaVector.push_back(a);
	a = new BossArea(player,0 + 17 + 17 + 17 + 17,0,0);
	a->createArea();
	areaVector.push_back(a);
	a = NULL;
}
void Level::update(double deltaTime)
{
	for(unsigned int i = 0; i < areaVector.size(); i++)
	{
		areaVector.at(i)->update(deltaTime);
	}
}
