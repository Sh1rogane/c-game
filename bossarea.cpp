#include "BossArea.h"

BossArea::BossArea(Player* player, double x, double y, double z)
{
	//ctor
	p = player;
	this->x = x;
	this->y = y;
	this->z = z;
	isFinish = false;
	isInside = false;
}

BossArea::~BossArea()
{
	//dtor
}
void BossArea::update(double deltaTime)
{
	if(!isInside && checkIfInside())
	{
		createEnemys();
		isInside = true;
	}
	if(isInside && entrance->getY() < y && !isFinish) 
	{
		closeDoors(deltaTime);
	}
	if(!isFinish && isInside&& checkIfFinish())
	{
		isFinish = true;
	}
	if(isFinish && entrance->getY() > y - 2 )
	{
		openDoors(deltaTime);
	}
}
void BossArea::baseRoom(float r, float g, float b)
{
	w = new Wall(x, y, z, 30, 4, 1,r,g,b);
	
	w = new Wall(x + 30, y, z, 1, 4, 6,r,g,b);
	w = new Wall(x + 30, y + 2, z + 6, 1, 2, 3,r,g,b);
	w = new Wall(x + 30, y, z + 9, 1, 4, 21,r,g,b);

	w = new Wall(x, y, z + 30, 31, 4, 1,r,g,b);
	
	w = new Wall(x, y, z, 1, 4, 6,r,g,b);
	
	w = new Wall(x, y + 2, z + 6, 1, 2, 3,r,g,b);
	
	w = new Wall(x, y, z + 9, 1, 4, 21,r,g,b);
	
	w = new Wall(x, y - 1, z, 30, 1, 30,r,g,b);
	w = NULL;
	
	entrance = new Wall(x, y - 2, z + 6, 1, 2, 3,r,g,b);
	
	exit = new Wall(x + 30, y - 2, z + 6, 1, 2, 3,r,g,b);
}
void BossArea::createArea()
{
	baseRoom(1.0,1.0,0.0);
}
void BossArea::createEnemys()
{
	e = new Boss(x + 15, y, z + 15);
	v.push_back(e);
}
