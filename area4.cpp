#include "Area4.h"

Area4::Area4(Player* player, double x, double y, double z)
{
	//ctor
	p = player;
	this->x = x;
	this->y = y;
	this->z = z;
	isFinish = false;
	isInside = false;
}

Area4::~Area4()
{
	//dtor
}
void Area4::update(double deltaTime)
{
	if(!isInside && checkIfInside())
	{
		createEnemys();
		isInside = true;
	}
	if(isInside && entrance->getY() < y && !isFinish) 
	{
		closeDoors(deltaTime);
	}
	if(!isFinish && isInside&& checkIfFinish())
	{
		isFinish = true;
	}
	if(isFinish && entrance->getY() > y - 2 )
	{
		openDoors(deltaTime);
	}
}
void Area4::createArea()
{
	baseRoom(1.0,0.0,1.0);
}
void Area4::createEnemys()
{
	e = new Enemy(this, x + 13, y, z + 5);
	v.push_back(e);
	e = new Enemy(this, x + 13, y, z + 13);
	v.push_back(e);
	e = new Enemy(this, x + 13, y, z + 8);
	v.push_back(e);
	e = new Enemy(this, x + 2, y, z + 3);
	v.push_back(e);
	e = new Enemy(this, x + 2, y, z + 13);
	v.push_back(e);
}
