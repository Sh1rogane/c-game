#ifndef AREA2_H
#define AREA2_H

#include "area.h"


class Area2 : public Area
{
	public:
		Area2(Player* player, double x, double y, double z);
		~Area2();
		void createArea();
		void update(double deltaTime);
	protected:
	private:
		void createEnemys();
};

#endif // AREA1_H
