#ifndef ENTITY_H
#define ENTITY_H
#include <GL/glut.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hitbox.h"
#include "global.h"

using namespace std;

class Entity
{
	
		
	protected:
		static const double PI = 3.1415926; //PI
		double x; //The x position
		double y; //The y position
		double z; //The z position
		double rotation; //The rotation of the object
		double height; //The height of the object
		double width; //The width of the object
		double depth; //The depth of the object
		Hitbox* hitbox; //The hitbox to handle collisions
		
	private:
	
	public:
		Entity();
		virtual ~Entity();
		virtual void render() = 0; //Main method to render objects
		virtual void update(double deltaTime) = 0; //Main method to update objects such as movements
		
		//Setters
		void setY(double newValue){y = newValue;}
		
		//Getters
		double getRotation()const{return rotation;}
		double getX()const{return x;}
		double getY()const{return y;}
		double getZ()const{return z;}
		double getWidth()const{return width;}
		double getHeight()const{return height;}
		double getDepth()const{return depth;}
		Hitbox* getHitbox()const{return hitbox;}
};

#endif // ENTITY_H
