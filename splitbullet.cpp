#include "splitbullet.h"

SplitBullet::SplitBullet(double x, double y, double z, double splitTime, int splitCount, double size, int rotation ,double speed)
{
	Global::array.push_back(this);
    this->x = x;
    this->y = y;
    this->z = z;
    this->rotation = rotation;
    
    this->splitTime = splitTime;
    this->splitCount = splitCount;
    
    timeSplit = 0;
    
    width = size;
    height = size;
    depth = size;
    this->speed = speed;
    calc();
    hitbox = new Hitbox(this, width, height, depth, width / 2, height / 2, depth / 2);
}
void SplitBullet::update(double deltaTime)
{
	//Adding the speed to the bullet so it will fly an direction.
    x += xSpeed * deltaTime;
    z += zSpeed * deltaTime;
    timeSplit += deltaTime;
    if(timeSplit > splitTime)
    {
    	split();
    }
}
/*
* Will remove the bullet and create splitCount number of bullets
*/
void SplitBullet::split()
{
	if(splitCount > 0)
	{
		int rotationSplit = 180 / splitCount;
	
		for(int i = 0; i < splitCount; i++)
		{
			b = new NormalBullet(x, y, z, 0.1, rotation + rotationSplit * i - 90, 20);
			b = NULL;
		}
	}
	
	delete this;
}
void SplitBullet::render()
{
	glPushMatrix();
	glColor3f(0, 0, 1);
	glTranslatef(x, y, z);
	glutSolidSphere(width,10,10);
	glPopMatrix();
	hitbox->updateHitbox();
}
SplitBullet::~SplitBullet()
{
	//dtor
}
