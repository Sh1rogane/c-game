#ifndef BOSSAREA_H
#define BOSSAREA_H

#include "area.h"
#include "boss.h"


class BossArea : public Area
{
	public:
		BossArea(Player* player, double x, double y, double z);
		~BossArea();
		void createArea();
		void update(double deltaTime);
	protected:
	private:
		void baseRoom(float r, float g, float b);
		void createEnemys();
};

#endif // BOSSAREA_H
