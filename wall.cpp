#include "wall.h"

Wall::Wall(double x, double y, double z, double width, double height, double depth, float r, float g, float b)
{

    //ctor
    this->r = r;
    this->g = g;
    this->b = b;
    
    this->x = x;
    this->y = y;
    this->z = z;
    this->width = width;
    this->height = height;
    this->depth = depth;
	hitbox = new Hitbox(this, width, height, depth, 0,0,0);

	Global::array.push_back(this);
}

Wall::~Wall()
{
    //dtor
}
void Wall::update(double deltaTime)
{
	//Checks if a bullet is hitting the wall
	for(unsigned int i = 0; i < Global::array.size(); i++)
	{
		Bullet* b = dynamic_cast<Bullet*>(Global::array[i]);
		if(b != NULL && hitbox->checkCollisions(b))
		{
			delete b;
		}
		b = 0;
	}
}
void Wall::render()
{
    glColor3f(r,g,b);
    glPushMatrix();

    glTranslatef(x + width / 2, y + height / 2, z + depth / 2);
    glScalef(width, height, depth);
    glutSolidCube(1.0);
    glColor3f(0.0,0.0,0.0);
    glutWireCube(1.0);
    glPopMatrix();

    hitbox->updateHitbox();
}
