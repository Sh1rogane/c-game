#ifndef MOB_H
#define MOB_H

#include "entity.h"
#include "global.h"
#include "normalbullet.h"
#include "bullet.h"


class Mob : public Entity
{
	public:
		Mob();
		~Mob();
		void render(){};
		void update(double deltaTime){};
		double getLife(){return life;}
		
		
	protected:
		double speed; //Speed of the mob
		//Witch direction the mob is going
		bool up;
		bool down;
		bool right;
		bool left;
	    double life;
	private:
};

#endif // MOB_H
