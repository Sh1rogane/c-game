#include "area.h"

Area::Area()
{
	//ctor
}

Area::~Area()
{
	//dtor
}
/*
* Creates a 15x15 room with the specified color
*/
void Area::baseRoom(float r, float g, float b)
{
	w = new Wall(x, y, z, 15, 4, 1,r,g,b);
	w = new Wall(x + 15, y, z, 1, 4, 6,r,g,b);
	w = new Wall(x + 15, y + 2, z + 6, 1, 2, 3,r,g,b);
	w = new Wall(x + 15, y, z + 9, 1, 4, 6,r,g,b);
	w = new Wall(x, y, z + 15, 16, 4, 1,r,g,b);
	w = new Wall(x, y, z, 1, 4, 6,r,g,b);
	w = new Wall(x, y + 2, z + 6, 1, 2, 3,r,g,b);
	w = new Wall(x, y, z + 9, 1, 4, 6,r,g,b);
	w = new Wall(x, y - 1, z, 15, 1, 15,r,g,b);
	w = NULL;
	entrance = new Wall(x, y - 2, z + 6, 1, 2, 3,r,g,b);
	exit = new Wall(x + 15, y - 2, z + 6, 1, 2, 3,r,g,b);
}
/*
* Returns true if the room is finish.
* By looking if the mob vector is empty
* and removes enemys from vector if theirs life is under 1
*/
bool Area::checkIfFinish()
{
	for(unsigned int i = 0; i < v.size(); i++)
	{
		if(v[i]->getLife() < 1)
		{
			v.erase(v.begin() + i);
		}
	}
	if(v.size() == 0)
	{
		return true;
	}
	
	return false;
}
/*
* Returns true if the player is inside the area/room
*/
bool Area::checkIfInside()
{
	return ((p->getX() > x + 3 ) && (p->getX() < x + 13 ) && (p->getZ() > z) && (p->getZ() < z + 15));
}
/*
* Closing the doors to the room
*/
void Area::closeDoors(double deltaTime)
{
	entrance->setY((entrance->getY()) + (deltaTime * 1));
	exit->setY((exit->getY()) + (deltaTime * 1));
}
/*
* Opening the doors to the room
*/
void Area::openDoors(double deltaTime)
{
	entrance->setY((entrance->getY()) - (deltaTime * 1));
	exit->setY((exit->getY()) - (deltaTime * 1));
}
void Area::createEnemys()
{
	
}
