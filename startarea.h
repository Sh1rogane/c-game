#ifndef STARTAREA_H
#define STARTAREA_H

#include "area.h"


class StartArea : public Area
{
	public:
		StartArea(double x, double y, double z);
		~StartArea();
		void createArea();
		void update(double deltaTime);
	protected:
	private:
		void createEnemys();
		void baseRoom(float r, float g, float b);
};

#endif // STARTAREA_H
