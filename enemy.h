#ifndef ENEMY_H
#define ENEMY_H

#include "mob.h"
#include "player.h"


class Area;

class Enemy : public Mob
{
	public:
		Enemy(Area* area, double x, double y, double z);
		~Enemy();
		void render();
		void update(double deltaTime);
	protected:
	private:
		bool reload;
		double moveTime;
		double reloadTime;
		Hitbox* findBox; //An hitbox that function is to see if the player is inside it 
		Area* area;
};

#endif // ENEMY_H
