#include "area3.h"

Area3::Area3(Player* player, double x, double y, double z)
{
	//ctor
	p = player;
	this->x = x;
	this->y = y;
	this->z = z;
	isFinish = false;
	isInside = false;
}

Area3::~Area3()
{
	//dtor
}
void Area3::update(double deltaTime)
{
	if(!isInside && checkIfInside())
	{
		createEnemys();
		isInside = true;
	}
	if(isInside && entrance->getY() < y && !isFinish) 
	{
		closeDoors(deltaTime);
	}
	if(!isFinish && isInside&& checkIfFinish())
	{
		isFinish = true;
	}
	if(isFinish && entrance->getY() > y - 2 )
	{
		openDoors(deltaTime);
	}
}
void Area3::createArea()
{
	baseRoom(0,0,0.5);
}
void Area3::createEnemys()
{
	e = new Enemy(this, x + 13, y, z + 5);
	v.push_back(e);
	e = new Enemy(this, x + 13, y, z + 13);
	v.push_back(e);
	e = new Enemy(this, x + 13, y, z + 8);
	v.push_back(e);
}
