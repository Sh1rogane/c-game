#ifndef NORMALBULLET_H
#define NORMALBULLET_H

#include "bullet.h"


class NormalBullet : public Bullet
{
    public:
        NormalBullet(double x, double y, double z, double size, int rotation , double speed);
        ~NormalBullet();
        void render();
    protected:
    private:
};

#endif // NORMALBULLET_H
